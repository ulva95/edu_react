export interface Author {
	id: number;
	fullname: string;
	position: string;
	affiliations: Affiliation[];
	grade: string;
}

export interface Affiliation {
	companyName: string;
	address: string;
	specialization: string;
}
