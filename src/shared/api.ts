import ky from "ky";
import { Author } from "./entities";

export const getAuthors = () =>
	ky.get("http://localhost:3001/authors").json<Author[]>();

export const getAuthor = (id: string) => {
	return ky.get(`http://localhost:3001/authors/${id}`).json<Author>();
};
