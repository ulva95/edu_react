//card component with title, description and button

import React, { FC } from "react";
import { Card, Button } from "react-bootstrap";
import { Author } from "../../../shared/entities";
import { Link } from "react-router-dom";

type Props = {
	author: Author;
};

const CardComponent: FC<Props> = ({ author }) => {
	return (
		<Card style={{ width: "18rem" }}>
			<Card.Body>
				<Card.Title>{author.fullname}</Card.Title>
				<Card.Text>{author.position}</Card.Text>

				<Link
					to={`/authors/${author.id}`}
					state={{ id: author.id }}
					title="Перейти"
				>
					<Button variant="primary">Перейти</Button>
				</Link>
			</Card.Body>
		</Card>
	);
};

export default CardComponent;
