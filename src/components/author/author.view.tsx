import React, { FC, useEffect, useState } from "react";
import { LinkProps, useLocation } from "react-router-dom";
import { getAuthor } from "../../shared/api";
import { Author } from "../../shared/entities";

export const AuthorView: FC = ({}) => {
	const location = useLocation();
	const [author, setAuthor] = useState<Author>({} as Author);
	useEffect(() => {
		getAuthor(location.state.id).then((res) => {
			setAuthor(res);
		});
	}, [location.state.id]);
	return (
		<div className="container-fluid m-10">
			<h1>{author.fullname}</h1>
			<div className="card">
				<div className="card-header">Информация</div>
				<div className="card-body">
					<blockquote className="blockquote mb-0">
						<p>Должность: {author.grade}</p>
						<p>Звание: {author.position}</p>
						<footer className="blockquote-footer"></footer>
					</blockquote>
				</div>
			</div>
			<div className="card mt-5">
				<div className="card-header">Аффилиации</div>
				<div className="card-body">
					{author.affiliations?.map((affiliation) => {
						return (
							<blockquote className="blockquote mb-0">
								<p>Название: {affiliation.companyName}</p>
								<p>Адрес: {affiliation.address}</p>
								<footer className="blockquote-footer">
									{affiliation.specialization}
								</footer>
							</blockquote>
						);
					})}
				</div>
			</div>
		</div>
	);
};
