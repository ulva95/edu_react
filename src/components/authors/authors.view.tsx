// list of authors , every author as a card

import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Card } from "../common/card";
import { getAuthors } from "../../shared/api";
import { Author } from "../../shared/entities";
import { log } from "console";

const AuthorsView: React.FC = () => {
	const [authors, setAuthors] = useState<Author[]>([]);

	useEffect(() => {
		getAuthors().then((authors) => setAuthors(authors));
		console.log(authors);
	}, []);

	return (
		<div className="container">
			<div className="row">
				{authors.map((author) => (
					<Card key={author.id} author={author} />
				))}
			</div>
		</div>
	);
};

export default AuthorsView;
