import React, { FC } from "react";
import { Link, Route, Routes } from "react-router-dom";
import { Authors } from "../authors";
import Author from "../author";

const LayoutView: FC = () => {
	return (
		<div className="container">
			<nav className="navbar navbar-expand-lg navbar-light bg-light">
				<ul className="navbar-nav">
					<li className="nav-item">
						<Link className=" nav-link active" to="/">
							Главная
						</Link>
					</li>
					<li className="nav-item">
						<Link className=" nav-link" to="/authors">
							Авторы
						</Link>
					</li>
				</ul>
			</nav>
			<div className="container-fluid">
				<Routes>
					<Route path="/authors" element={<Authors />} />
					<Route path="/authors/:id" element={<Author />} />
				</Routes>
			</div>
			<footer className="footer bg-dark w-100 py-3 fixed-bottom">
				<div className="container">
					<span className="text-muted">Footer</span>
				</div>
			</footer>
		</div>
	);
};

export default LayoutView;
