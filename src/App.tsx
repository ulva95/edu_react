import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { Layout } from "./components/layout";
import { BrowserRouter } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";

function App() {
	return (
		<div className="App">
			<BrowserRouter>
				<Layout />
			</BrowserRouter>
		</div>
	);
}

export default App;
